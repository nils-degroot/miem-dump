<?php

namespace App\Http\Controllers\api;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class FileController extends Controller
{
    public function index(Request $request)
    {
        //return File::paginate(10);

        $files = File::orderBy('created_at', 'DESC')->paginate(10);
        $files->getCollection()->transform(function ($file) {
            return [
                'name' => $file->name,
                'file_url' => asset(Storage::url('public/miem/' . $file->uuid . '.' . $file->extension))
            ];
        });

        return $files;
    }

    public function store(Request $request)
    {
        $uuid = Uuid::uuid4();

        $request->file('file')->storeAs('public', 'miem/' . $uuid . '.' . $request->file('file')->getClientOriginalExtension());
        File::create([
            'uuid' => $uuid,
            'name' => $request->get('name', ''),
            'extension' => $request->file('file')->getClientOriginalExtension()
        ]);

        return [];
    }

    public function show($uuid)
    {
        return File::where('uuid', '=', $uuid)->first();
    }

    public function update(Request $request, $uuid)
    {
        //
    }

    public function destroy($uuid)
    {

    }
}
