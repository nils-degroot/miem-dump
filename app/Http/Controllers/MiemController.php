<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiemController extends Controller
{
    public function index()
    {
        return view('miem.index');
    }

    public function add()
    {
        return view('miem.upload');
    }
}
