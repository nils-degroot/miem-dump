<?php

use App\File;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class FilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loops = 50;

        for ($i = 0; $i < $loops; $i++) {
            File::create([
                'uuid' => Uuid::uuid4(),
                'name' => Faker\Provider\Lorem::word()
            ]);
        }
    }
}
