<html>
    <head>
        <title>Miem dump</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="/css/app.css" />
    </head>
    <body class="bg-black">
        <div class="container mt-3">
            <h2 class="text-white text-center mb-0">
                <span class="title font-weight-bold">MIEM DUMP</span>
            </h2>
            <div class="row justify-content-center">
                @yield('content')
            </div>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
