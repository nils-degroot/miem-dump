/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');
require('jquery');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./components/App');

$(document).ready(function () {
    $('#miemForm').on('submit', function (e) {
        e.preventDefault();
        let fileInput = $("#miemInput");

        let form = new FormData();
        form.set("file", fileInput.prop("files")[0], fileInput.val().split("/").pop());
        form.set("_token", $("#token").val());
        form.set("name", $("#nameInput").val());

        let uploadXHR = new XMLHttpRequest();
        uploadXHR.open("POST", "/api/file");
        uploadXHR.send(form);

        uploadXHR.addEventListener('readystatechange', (e) => {
            if (uploadXHR.readyState === uploadXHR.DONE) {
                console.log(uploadXHR.responseText);
            }

            window.location = '/'
        });

        uploadXHR.addEventListener('error', (e) => {
            console.log('Request failed')
        })


    });
});
